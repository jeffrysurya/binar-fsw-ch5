const http = require('http');
const fs = require('fs');

function onRequest (req, res) {
    res.writeHead(200, {'Content-Type': "text/html"})
    fs.readFile('./todo.txt',null,(error, data) => {
        if (error) {
            res.writeHead(404);
            res.write('file not found');
        }
        console.log(data)
        res.write (data)
    }, res.end()
    )
}

http.createServer(onRequest).listen(8000)